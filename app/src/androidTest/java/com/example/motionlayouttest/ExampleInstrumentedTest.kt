package com.example.motionlayouttest

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)


    @Test
    fun clickFab_UpdatesUi() {
        runBlocking { delay(1000) } // delay to make sure included in test lab video

        (0 until 10).forEach {
            if (it % 2 == 0) {
                onView(withId(R.id.textview)).check(matches(withText("@@")))
            } else {
                onView(withId(R.id.textview)).check(matches(withText("\$\$")))
            }
            onView(withId(R.id.fab)).perform(click())
            runBlocking { delay(1000) } // delay to make sure included in test lab video
        }
    }
}